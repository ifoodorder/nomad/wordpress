job "wordpress" {
	datacenters = ["dc1"]
	type        = "service"
	group "wordpress" {
		network {
			port "https" {
				to = 443
			}
		}
		service {
			name = "wordpress"
			tags = ["front"]
			port = "https"
		}
		task "wordpress" {
			resources {
				cpu    = 500
				memory = 500
			}
			vault {
				policies = ["wordpress"]
			}
			template {
        data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=wordpress" "common_name=wordpress.service.consul" "ttl=24h" "alt_names=_wordpress._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.ca_chain }}
{{ .Data.certificate }}
{{ end }}{{ end }}
EOH
				destination = "local/fullchain.pem"
			}
			template {
				data =<<EOH
{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=wordpress" "common_name=wordpress.service.consul" "ttl=24h" "alt_names=_wordpress._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}
EOH
				destination = "secrets/privkey.pem"
			}
			template {
				data        = <<EOH
				WORDPRESS_DB_HOST = mariadb.service.consul:3306
				WORDPRESS_DB_USER = {{ with secret "database/creds/wordpress" }}{{ .Data.username }}
				WORDPRESS_DB_PASSWORD = {{ .Data.password }}{{ end }}
				WORDPRESS_DB_NAME = wordpress
				EOH
				destination = "secrets/file.env"
				env         = true
			}
			template {
				data        = <<EOH
#!/bin/sh
a2enmod ssl
cp /local/apache-ssl.conf /etc/apache2/sites-enabled/000-ssl.conf
docker-entrypoint.sh apache2-foreground
EOH
				destination = "local/apache-ssl.sh"
				perms       = 755
			}
			template {
				data        = <<EOH
<VirtualHost *:443>
	DocumentRoot /var/www/html
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	SSLEngine on
	ServerName wordpress.service.consul
	SSLCertificateFile /local/fullchain.pem
	SSLCertificateKeyFile /secrets/privkey.pem
	<FilesMatch "\.(cgi|shtml|phtml|php)$">
		SSLOptions +StdEnvVars
	</FilesMatch>
	<Directory /usr/lib/cgi-bin>
		SSLOptions +StdEnvVars
	</Directory>
</VirtualHost>
EOH
				destination = "local/apache-ssl.conf"
			}
			driver = "docker"
			config {
				image      = "wordpress:5"
				entrypoint = ["/local/apache-ssl.sh"]
				ports      = ["https"]
			}
			volume_mount {
				volume      = "data"
				destination = "/var/www/html"
			}
		}
		volume "data" {
			type      = "host"
			source    = "wordpress"
			read_only = false
		}
	}
}
